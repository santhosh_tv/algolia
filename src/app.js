/* global instantsearch algoliasearch */

const search = instantsearch({
  indexName: 'demo_ecommerce',
  searchClient: algoliasearch('B1G2GM9NG0', 'aadef574be1f9252bb48d4ea09b5cfe5'),
});

search.addWidget(
  instantsearch.widgets.searchBox({
    container: '#searchbox',
  })
);

search.addWidget(
  instantsearch.widgets.clearRefinements({
    container: '#clear-refinements',
  })
);

search.addWidget(
  instantsearch.widgets.refinementList({
    container: '#brand-list',
    attribute: 'brand',
  })
);

search.addWidget(
  instantsearch.widgets.hits({
    container: '#hits',
    templates: {
      item: `
        <div>
          <img src="{{degreeimage}}" align="left" alt="{{code}}" />
          <div class="hit-name">
            {{#helpers.highlight}}{ "attribute": "code" }{{/helpers.highlight}}}}
          </div>
          <div class="hit-description">
            {{#helpers.highlight}}{ "attribute": "code" }{{/helpers.highlight}}}}
          </div>
          <div class="hit-price">\${{degreeimage}}</div>
        </div>
      `,
    },
  })
);

search.addWidget(
  instantsearch.widgets.pagination({
    container: '#pagination',
  })
);

search.start();
